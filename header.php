<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">   
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">

    <title>.::DFL Book.Com::.</title>

    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
	<!-- slick css-->
	<link rel="stylesheet" href="slick/slick.css" type="text/css">
	<link rel="stylesheet" href="slick/slick-theme.css" type="text/css">
	
	 <!-- Bootstrap Dropdown Hover CSS -->
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/bootstrap-dropdownhover.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/navbar-fixed-top.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  
	<body class="body" data-spy="scroll" data-target=".navbar" data-offset="60">
	<div class="headhigh">
		<div class="container">
			<div class="row">
				<div class="col-md-6 text-left">
					<ul class="Numem">
						<li><a href="#"><i class="fa fa-phone"></i> +880 1829962594</a></li>
						<li><a href="#"><i class="fa fa-envelope"></i> info@dflbook.com</a></li>
					</ul>
				</div>
				<div class="col-md-6 social">
					<ul class="nav navbar-nav">
						<li style="padding-top:6px;color:#fff;"><span class="dt">2 November, 2018 &nbsp; |</span></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div class="overlay">
		<!-- Scroll Me -->
		<nav class="navbar navbar-default">
		  <div class="container">
			<div style="padding-top:0px; margin-top:0px;" class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="index.php"><img class="" src="img/logo.jpg" alt="logo"/></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="index.php">Home</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Contact</a></li>
					<li><a href="#">Free Service</a></li>
					<li><a href="#">Education</a></li>
					<li><a href="#">Tour</a></li>
					<li><a href="#">Achievement</a></li>
					<li><a href="#">Affiliate</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<form style="padding-top:8px;">
							<input type="text" name="search" id="search" placeholder="Search here..." class="form-control"/>
						</form>
					</li>
					<li><a href="login.php">Log In</a></li>
				</ul>
			  
			</div><!--/.nav-collapse -->
		  </div>
		</nav>
		<!-- Get Me-->
		<nav class="nav-scroll navbar navbar-default navbar-fixed-top">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="index.php"><img class="" src="img/logo.jpg" alt="logo"/></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">
					<li class="active"><a href="index.php">Home</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Contact</a></li>
					<li><a href="#">Free Service</a></li>
					<li><a href="#">Education</a></li>
					<li><a href="#">Tour</a></li>
					<li><a href="#">Achievement</a></li>
					<li><a href="#">Affiliate</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<form style="padding-top:8px;">
							<input type="text" name="search" id="search" placeholder="Search here..." class="form-control"/>
						</form>
					</li>
					<li><a href="login.php">Log In</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		  </div>
		</nav>
	</div>