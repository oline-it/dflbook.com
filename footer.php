<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="companyinfo">
							<h2>DFLBook.com</h2>
						</div>
					</div>
					<div class="col-sm-6 cardinfo">
						<h3>Our Partners :</h3>
						<div align="center"><img class="thumbnail" src="img/conA.png" width="100%" height="auto"/>
						%sd=";
						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="images/home/map.png" alt="" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-sm-3">
								<div class="single-widget">
									<h2>Service</h2>
									<ul>
										<li><a href="#">Online Help</a></li>
										<li><a href="#">Contact Us</a></li>
										<li><a href="#">Order Status</a></li>
										<li><a href="#">Change Location</a></li>
										<li><a href="#">FAQ’s</a></li>
									</ul>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="single-widget">
									<h2>Quock Shop</h2>
									<ul>
										<li><a href="#">T-Shirt</a></li>
										<li><a href="#">Mens</a></li>
										<li><a href="#">Womens</a></li>
										<li><a href="#">Gift Cards</a></li>
										<li><a href="#">Shoes</a></li>
									</ul>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="single-widget">
									<h2>Policies</h2>
									<ul>
										<li><a href="#">Terms of Use</a></li>
										<li><a href="#">Privecy Policy</a></li>
										<li><a href="#">Refund Policy</a></li>
										<li><a href="#">Billing System</a></li>
										<li><a href="#">Ticket System</a></li>
									</ul>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="single-widget">
									<h2>About Shopper</h2>
									<ul>
										<li><a href="#">Company Information</a></li>
										<li><a href="#">Careers</a></li>
										<li><a href="#">Store Location</a></li>
										<li><a href="#">Affillate Program</a></li>
										<li><a href="#">Copyright</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="single-widget">
							<h2>Our Address</h2>
							<p class="text-left">
								Jahan Chember (7th Floor), Chowmuhuni Road<br/>
								North Agrabad, Chittagong<br/>
								Mobile: 01829-962 594<br/>
								Email : dfleventzone@gmail.com<br/>
								Web	: www.dflbook.com
							</p>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2018 DFL Book.com | All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.olineit.com">Oline IT</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
		<script src="js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<script type="text/javascript" src="js/scrollme.js"></script>
		<!-- Bootstrap Dropdown Hover JS -->
		<script src="js/bootstrap-dropdownhover.min.js"></script>
		<!--slick js-->
		<script type="text/javascript" src="slick/jquery-1.8.2.min.js"></script>
		<script type="text/javascript" src="slick/migrate.js"></script>
		<script type="text/javascript" src="slick/slick.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
			  $('.spnslider').slick({
				 centerMode: true,
				 dots: true,
				 autoplay: false,
				 autoplaySpeed: 2000,
				 centerPadding: '60px',
				 slidesToShow: 1,
				});
		});
	</script>
	</body>
</html>