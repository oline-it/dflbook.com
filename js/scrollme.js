(function ($) {
  $(document).ready(function(){

    // hide .navbar first
    $(".nav-scroll").hide();

    // fade in .navbar
    $(function () {
        $(window).scroll(function () {

                 // set distance user needs to scroll before we start fadeIn
            if ($(this).scrollTop() > 100) {
                $('.nav-scroll').fadeIn();
            } else {
                $('.nav-scroll').fadeOut();
            }
        });
    });

});
  }(jQuery));