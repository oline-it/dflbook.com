<div id="con">
	<div class="container">
		<div class="row">
			<div id="cat" class="col-md-2 left thumbnail">
				<h3>Category</h3>
				<ul>
					<li><a href="#">DFLBazaar.com</a></li>
					<li><a href="#">DFLRide.com</a></li>
					<li><a href="#">DFLJobs.com</a></li>
					<li><a href="#">DFLKroy.com</a></li>
					<li><a href="#">DFLBooking.com</a></li>
					<li><a href="#">DYTA.com</a></li>
					<li><a href="#">DFLNews.com</a></li>
					<li><a href="#">Doctor</a></li>
					<li><a href="#">SME উদ্যেক্তা </a></li>
					<li><a href="#">C&F & Shipping </a></li>
					<li><a href="#">Discount Outlet 2000+ </a></li>
					<li><a href="#">Export & Import </a></li>
					<li><a href="#">BD All Factory List </a></li>
					<li><a href="#">Emergency Contant Num. </a></li>
					<li><a href="#">Mobile Recharge </a></li>
					<li><a href="#">Mobile Banking </a></li>
					<li><a target="_blank" href="https://www.facebook.com/dfleventzone/">DFL Tour & Event Zone</a></li>
					<li><a target="_blank" href="https://www.youtube.com/channel/UCy97_8WrCRLOjsED6MyMjOw">DFL TV.com </a></li>
				</ul>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1"></li>
							<li data-target="#carousel-example-generic" data-slide-to="2"></li>
							<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
							<div class="item active">
							  <img src="img/slider/1.jpg" alt="slide1">
							  <div class="carousel-caption">
									<h2>Learning & Earning</h2>
							  </div>
							</div>
							<div class="item">
							  <img src="img/slider/2.jpg" alt="slide2">
							  <div class="carousel-caption">
									<h2>Change your life today</h2>
							  </div>
							</div>
							<div class="item">
							  <img src="img/slider/3.jpg" alt="slide3">
							  <div class="carousel-caption">
								<h2>Learn More & Earn More</h2>
							  </div>
							</div>
							<div class="item">
							  <img src="img/slider/4.jpg" alt="slide4">
							  <div class="carousel-caption">
								
							  </div>
							</div>
						  </div>

						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
					<div class="col-md-12">
						<img style="margin-top:20px;margin-bottom:20px;" class="thumbnail" src="img/conA.png" width="100%" alt="sponsor"/>
					</div>
					<hr/>
					<div class="col-md-12">
						<div class="subm"
						  <!-- Nav tabs -->
						  <ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#pop" aria-controls="pop" role="tab" data-toggle="tab">Popular</a></li>
							<li role="presentation"><a href="#fserv" aria-controls="fserv" role="tab" data-toggle="tab">Free Service</a></li>
							<li role="presentation"><a href="#new" aria-controls="new" role="tab" data-toggle="tab">New</a></li>
							<li role="presentation"><a href="#allsv" aria-controls="allsv" role="tab" data-toggle="tab">All Service</a></li>
						  </ul>
						  <!-- Tab panes -->
						  <div class="tab-content">
							<div style="padding:10px;" role="tabpanel" class="tab-pane active" id="pop">
								<div class="row">
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/1.png"/>
											<div class="">
												dflbazar.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/2.png"/>
											<div class="">
												dflRide.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/3.png"/>
											<div class="">
												dflBooking.com
											</div>
										</div>
									</a>
									
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/4.png"/>
											<div class="">
												dflDoctor
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/5.png"/>
											<div class="">
												YouthTraining
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/6.png"/>
											<div class="">
												dflNews.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/7.png"/>
											<div class="">
												dflbazar.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/8.png"/>
											<div class="">
												Recharge
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/9.png"/>
											<div class="">
												MobileBanking
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/10.png"/>
											<div class="">
												Tour&Event
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/11.png"/>
											<div class="">
												dfl-TV.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/12.png"/>
											<div class="">
												Em.Contact
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/13.png"/>
											<div class="">
												FactoryList.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/14.png"/>
											<div class="">
												dflSME.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/15.png"/>
											<div class="">
												dflJobs.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/16.png"/>
											<div class="">
												dflKroy.com
											</div>
										</div>
									</a>
								</div>
							</div>
							<div style="padding:10px;" role="tabpanel" class="tab-pane" id="fserv">
								<div class="row">
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/11.png"/>
											<div class="">
												dfl-TV.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/12.png"/>
											<div class="">
												Em.Contact
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/13.png"/>
											<div class="">
												FactoryList.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/14.png"/>
											<div class="">
												dflSME.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/15.png"/>
											<div class="">
												dflJobs.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/16.png"/>
											<div class="">
												dflKroy.com
											</div>
										</div>
									</a>
								</div>
							</div>
							<div style="padding:10px;" role="tabpanel" class="tab-pane" id="new">
								<div class="row">
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/1.png"/>
											<div class="">
												dflbazar.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/2.png"/>
											<div class="">
												dflRide.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/3.png"/>
											<div class="">
												dflBooking.com
											</div>
										</div>
									</a>
									
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/4.png"/>
											<div class="">
												dflDoctor
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/5.png"/>
											<div class="">
												YouthTraining
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/6.png"/>
											<div class="">
												dflNews.com
											</div>
										</div>
									</a>
								</div>
							</div>
							<div style="padding:10px;" role="tabpanel" class="tab-pane" id="allsv">
								<div class="row">
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/1.png"/>
											<div class="">
												dflbazar.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/2.png"/>
											<div class="">
												dflRide.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/3.png"/>
											<div class="">
												dflBooking.com
											</div>
										</div>
									</a>
									
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/4.png"/>
											<div class="">
												dflDoctor
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/5.png"/>
											<div class="">
												YouthTraining
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/6.png"/>
											<div class="">
												dflNews.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/7.png"/>
											<div class="">
												dflbazar.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/8.png"/>
											<div class="">
												Recharge
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/9.png"/>
											<div class="">
												MobileBanking
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/10.png"/>
											<div class="">
												Tour&Event
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/11.png"/>
											<div class="">
												dfl-TV.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/12.png"/>
											<div class="">
												Em.Contact
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/13.png"/>
											<div class="">
												FactoryList.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/14.png"/>
											<div class="">
												dflSME.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/15.png"/>
											<div class="">
												dflJobs.com
											</div>
										</div>
									</a>
									<a href="#">
										<div class="col-md-2 text-center">
											<img src="img/icon/16.png"/>
											<div class="">
												dflKroy.com
											</div>
										</div>
									</a>
								</div>
							</div>
						  </div>
						</div>
					</div>
				</div>
			</div>
			<div style="padding:0px;" class="col-md-2">
				<img class="thumbnail" width="100%" src="img/adm/myad2.jpg"/>
				<img class="thumbnail" width="100%" src="img/adm/1.jpg"/>
			</div>
		</div>
	</div>
</div>